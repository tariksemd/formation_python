#coding: utf-8 
from _signal import signal
capteur1 = {"fleche", "bord", "arrete"}
capteur2 = {"fleche", "bord"}
capteur3 = {"sortie 2", "entree", "fleche"}


signaux = [capteur1,capteur2, capteur3]

res = capteur1
res_ou = capteur1
res_xor =capteur1

#premier cas
print("Signaux actifs sur tous les capteurs :")
for capteur in signaux[1:]:
    res = res & capteur
print(res)

#deuxieme cas
print("Signaux actifs au moins un capteur :")
for capteur in signaux[1:]:
    res_ou = res_ou | capteur
print(res_ou)

#3eme cas
print("Signaux actifs sur un nombre impair de capteurs :")
for capteur in signaux[1:]:
    res_xor = res_xor ^ capteur
print(res_xor)

"""
print({1,2} & {1,3})
print({1,2} | {1,3})
print({1,2} - {1,3})
print({1,2} ^ {1,3})


Algebre ensembliste
{1,2} & {1,3}
{1}
{1,2} | {1,3}
{1, 2, 3}
{1,2} - {1,3}
{2}
{1,2} ^ {1,3}
{2, 3}
"""

print("Nombre de fois qu'apparait chaque signal :")

dic ={} # clés  str, les valeurs int
cmp = 0
koo = "fleche"
#print(signaux)

for capteur in signaux:
    for data in capteur:
        if data in dic:
            dic[data] += 1
        else:
            dic[data]  = 1
print(dic)
        

tout = {"signaux":signaux,
        "resultats":{
            "sur tous":res,
            "sur au moins":res_ou,
            "sur impair":res_xor,
            "occurreces":dic}
    }
print((tout))
