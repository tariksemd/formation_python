#coding: utf-8
import re
class Correction:
    def mettre_en_miniscules(self,s):
        """Met en minuscules uniquement la chaine
        >>> c = Correction()
        >>> c.mettre_en_miniscules('Hello !')
        'hello !'
        >>> s = c.mettre_en_miniscules('ABC')
        >>> s[0]
        'a'
        >>> s[1]
        'b'
        """
        return s.lower()
    
    
    def enlever_telephones(self, s):
        """Remplacer les numéro de tel par des étoiles (i.e. : "redaction")
        >>> c = Correction()
        >>> res = c.enlever_telephones("0765696569 est le numéro de Mr Boba")
        >>> res
        '*** est le numéro de Mr Boba'
        """
        return re.sub("[0-9]+", "***",s)
    
    def creer_attribut(self):
        """Test dans le cas d'une fonction sans return
        >>> c = Correction()
        >>> c.creer_attribut()
        >>> c.attributl
        19
        
        """
        self.attributl = 19
    
if __name__== "__main__":
    import doctest
    doctest.testmod()
    print("Tests finis !")