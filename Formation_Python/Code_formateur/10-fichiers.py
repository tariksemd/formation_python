# pour que ça marche dans Notepad++ : 
# [ cmd /C cd $(CURRENT_DIRECTORY) && python -i $(FULL_CURRENT_PATH) ]

# creer logcat25390.txt a partir de logcat.txt (seulement les lignes processusparent=25390)
fr = open("logcat.txt", "r")
fr.seek(0) # se place au début (ici : inutile, déja au début)
fw = open("logcat25390.txt", "w")
for l in fr:
    elements = l.split()
    if len(elements)>1 and elements[2]=="25390":
        fw.write(l)
fw.close()
fr.close()
print("Copié filtré")

# Créer logcat_parprocessus.txt à partir de logcat.txt (regroupe les lignes par proc. parent)

# solution 1 : tout enregistrer en mémoire, organisé (un dictionnaire, une liste...), puis
# écrire, regrouper dans l'ordre
# défaut : fichier de 1Go = mémoire de 1Go (voire +)


# solution 2 : idem, mais dans des fichiers isolés (un par processus)
# défaut : fichier de 10000 pid (processus) = 10000 fichiers créés / ouverts


# solution 3 : relire le fichier une fois par numéro de processus
# défaut : complexité n**1.5


# solution 4 : réaliser une liste des pid et longueur de chaque ligne, puis reconstituer le
# fichier final en se déplaçant au bon endroit dans le fichier source
# défaut : nombreux déplacements dans le fichier source
fr = open("logcat.txt", "r")
liste = []
pids = set()
pos = 0
for ligne in fr:
    longueur = len(ligne)+1
    elements = l.split()
    if len(elements)>1:
        pid = elements[2]
        liste.append({ "pos": pos, "pid": elements[2], "longueur":longueur })
        pids.add(elements[2])
    pos += longueur
fr.close()
fr = open("logcat.txt", "r")
fw = open("logcat_parprocessus.txt", "w")
for pid in pids:
    for ligne in liste:
        if ligne["pid"]==pid:
            fr.seek(ligne["pos"])
            fw.write(fr.readline())
fw.close()
fr.close()
print("Copié regroupé")


# ouverture d'un fichier binaire
frb = open(".git\\index", "rb");
contenu = frb.read()
print("Premier octet : "+str(contenu[0])) # 68
frb.close()
