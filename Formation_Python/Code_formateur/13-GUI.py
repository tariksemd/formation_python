"""Fenetre TK"""
import tkinter

class ModificationFrame(tkinter.Frame):
    def __init__(self, master):
        tkinter.Frame.__init__(self, master=master, width=500, height=500)
        self.pack()
        
        self.entry1 = tkinter.Entry(self, text="Entrer ici")
        self.entry1.pack()
        self.entry1.place(x=10, y=10, width=200, height=40)
        
        self.bouton1 = tkinter.Button(self, text="Appuyer là pour mettre en minuscules", 
            command=self.bouton1_click)
        self.bouton1.pack()
        self.bouton1.place(x=10, y=70, width=200, height=50)
        
        self.mainloop()
        
    def bouton1_click(self):
        texte = self.entry1.get().lower()
        self.entry1.delete(0,"end")
        self.entry1.insert(0,texte)        

if __name__=="__main__":
    afficheur = tkinter.Tk()
    fenetre = ModificationFrame(afficheur)


