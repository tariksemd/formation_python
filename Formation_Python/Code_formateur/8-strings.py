import re 

# Traitements sur les chaines
textes = (
    "Ce texte est un texte normal en français, sans particularités, si ce n'est peu d'accents",
    "This is an english text, well written by an english person",
    "My tailor is rich, Ben is in the kitchen, Look, it's a plane, a bird, no it's superman"
)

# differences (info statistique, cf : https://en.wikipedia.org/wiki/Letter_frequency )
# anglais : 2% de w, 2% de y, 2.7% de u, 0.1% de q, 6% de h dans un texte sans espaces
# français : 0% de w, 0.1% de y, 6% de u, 1.4% de q, 0.6% de h
# algorithme qui dit si chaque texte est probablement français ou anglais :
stats_prc = {
    'fr':{'w':0, 'y':0.1, 'u':6, 'q':1.4, 'h':0.6},
    'en':{'w':2, 'y':2, 'u':2.7, 'q':0.1, 'h':6}
}

lettres_utiles = ('h','q','u','w','y')
for texte in textes:
    pourcents = {}
    for l in lettres_utiles:
        pourcents[l] = 0
    for c in texte.lower():
        if c in lettres_utiles:
            pourcents[c] += 1/len(texte.replace(' ',''))
    print(" * "+texte)
    print(pourcents)
    if pourcents['h']>pourcents["u"]:
        print("Anglais")
    else:
        print("Français")
        
print("Syntaxe : ")
# indiquer (True/False) si la syntaxe est correcte : toute virgule est suivie d'un espace
for texte in textes:
    ok = True
    for i in range(len(texte)-1):
        if texte[i] == ',' and texte[i+1] != ' ':
            ok = False
    print(texte+" : "+str(ok))

print("Sujet : ")
# indiquer si on trouve "text" ou "mot" ou "word" dans la phrase    
for texte in textes:
    linguistique = "text" in texte or "mot" in texte or "word" in texte
    print(texte+" : "+str(linguistique))

print("Simple : ")
# grace au regexp : indiquer si c'est des textes simples :
# - aucun mot de plus de 9 lettres
# - aucun tiret (-), point-virgule (;), guillemet (")
for texte in textes:
    simple = re.match(".*[A-Za-z]{10}", texte) == None
    if simple:
        simple = re.match(".*[;\"\-]", texte) == None
    print(texte+" : "+str(simple))