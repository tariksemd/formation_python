# Acces à une base SQLite 3
import sqlite3
import datetime
conn = sqlite3.connect("log.sqlite")
conn.execute("CREATE TABLE IF NOT EXISTS messages(id INTEGER PRIMARY KEY, info TEXT)")

# écriture
info = "Il est : "+str(datetime.datetime.now())
conn.execute("INSERT INTO messages(info) VALUES(?)", (info,) )
conn.commit()

# lecture
c = conn.cursor()
c.execute("SELECT * FROM messages")
for ligne in c.fetchall():
    print(ligne)
c.close()

conn.close()