# un operateur lit un code d'erreur sur un appareil et l'analyse ici
try:
    n = int(input("Code ?"))
    print("Code d'erreur : ", n)
    
    # exemple :
    # 1 => erreurs 1
    # 2 => erreurs 2
    # 4 => erreurs 3
    # 8 => erreurs 4
    # 5 => erreurs 1 et 3 (00000001 et 00000100)
    # 10 => erreurs 2 et 4
    # 7 => erreurs 1 et 2 et 3
    num = 0
    bit = 1
    s = ""
    sep = ""
    # complexité : O(log(n))
    while num < n:
        num = 2**(bit-1)
        if num & n != 0:
            s += sep + str(bit)
            sep = " et "
        bit += 1
    print(s)
    # complexités dans l'ordre : O(1), O(log(n)), O(n), O(n*log(n)), O(n²), O(n**3)...
    
except ValueError:
    print("ValueError !")
    
    
