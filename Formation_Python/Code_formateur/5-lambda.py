# >>> f5 = lambda x,y: x*y
# >>> f5(2,3)
# 6
# >>> l1 = [3,4,3,2,1,4]
# >>> l2 = filter(lambda x:x<3, l1)
# >>> list(l2)
# [2, 1]
data = (2.3, 4.0, 6.5, 1.0, 3.4)
filtered_data = list(filter(lambda v:int(v)%2==0, data))
print(filtered_data) # 2.3, 4.0, 6.5 (nombres dont la partie entière est paire)