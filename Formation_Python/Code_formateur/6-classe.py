"""Module de test des multiples classes"""

class Personne:
    """Exemple de classe complete.
    Avec constructeur, méthode et même surcharge d'opérateur.
    """
    
    def __init__(self,n):
        """Constructeur 
        
        self : objet en cours
        n : nouveau nom de la personne
        """
        self.nom = n
        self.age = 10
    
    def vieillir(self):
        self.age += 1
    
    def __add__(self,v):
        p2 = Personne(self.nom)
        p2.age = self.age+v
        return p2

class Marin(Personne):
    def __init__(self,n):
            Personne.__init__(self,n)
            self.bateau = ""

#p1 = Personne()
#print(p1.age) # 10
#p1.vieillir()
#print(p1.age) # 11
# Ajouter "DoubleCapteur", un Capteur avec une valeur2 >= valeur
class Capteur:
    """Classe mère"""
    def __init__(self, nom = "", min = 0, valeur = 0, max = 100):
        self.nom = nom
        self.min = min
        self.valeur = valeur
        self.max = max

    def afficher(self):
        str = self.nom+" |"
        for n in range(10):
            if n<(self.valeur-self.min) * 10 / (self.max-self.min):
                str += "#"
            else:
                str += "-"
        str += "|"
        print(str)

    def __mul__(self, v):
        return Capteur(self.nom, self.min*v, self.valeur*v, self.max*v)

    def __truediv__(self, v):
        #return Capteur(self.nom, self.min/v, self.valeur/v, self.max/v)
        return self * (1/v)

class DoubleCapteur(Capteur):
    def __init__(self, nom = "", min = 0, valeur = 0, valeur2 = 0, max = 100):
        Capteur.__init__(self, nom, min, valeur, max)
        self.valeur2 = valeur2
        
    def afficher(self):
        str = self.nom+" |"
        for n in range(10):
            if n<(self.valeur-self.min) * 10 / (self.max-self.min):
                str += "#"
            elif n<(self.valeur2-self.min) * 10 / (self.max-self.min):
                str += "o"
            else:
                str += "-"
        str += "|"
        print(str)

    def __mul__(self, v):
        return DoubleCapteur(self.nom, self.min*v, self.valeur*v, self.valeur2*v, self.max*v)

# si j'exécute le fichier (> python 6-classe) : __name__ contient "__main__"
# si j'importe le fichier (import 6-classe, > pydoc... 6-classe) : __name__ contient "6-classe"
if __name__=="__main__":
    c1 = Capteur()
    c1.valeur = 66
    c1.afficher()
    c2 = Capteur("Capteur 42", -5, 2, 5) * 3 / 2
    c2.afficher()
    print(c2.valeur) # 3
    dc1 = DoubleCapteur("Capteur double 8", 0, 20, 40, 100)
    dc1.afficher()
    dc1 = dc1 / 5 # DC/ -> C/ -> DC*