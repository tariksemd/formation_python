values = [4, 2, 10, 9, 7, 8, 4, 3, 2, 4, 3, 9, 6, 3, 10, 11, 21]

print("Afficher tous les cas où deux valeurs se suivent")
# ici : 7 10
# complexité : O(n)
for i in range(len(values)-1):
    if values[i]==values[i+1]-1:
        print(values[i])

# complexité : O(n²)
for i in range(len(values)-1):
    for i in range(len(values)-1):
        if values[i]==values[i+1]-1:
            print(values[i])

print("Afficher les valeurs pour lesquelles les 2 suivantes existent aussi dans la liste")
# ici : 2 9 7 8 2 6 9
# complexité : O(2n²)
for v in values:
    if (v+1 in values) and (v+2 in values):
        print(v)
        
print("Mieux : complexite : O(n*log(n))")
values.sort()
trou = values[0]-1
for i in range(1, len(values)):
    if values[i-1]<values[i]-1:
        trou = values[i]-1
    if values[i]>trou+2 and values[i-2]>values[i-3]:
        print(values[i]-2)
    
    
    