#coding: utf-8 
print('Héllo world')
b=100
try:
    print("Ok")
    b = b/0
    print("ok 2")
except ZeroDivisionError as ex:
    print("arg!", ex)
except:
    print('Erreur inattendue')
    # The default globla must in the end of except.
    """
    le else s'éxécute meme si y a pas de try
    """
else:
    print('fin')    
    """
    ce code va etre executer de toute facon.
    quoi qu'il arrive fait ça.
    """    
    
finally:
    print('.')