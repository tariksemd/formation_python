#coding: utf-8
#Fenetre TK
import tkinter

class ModificationFrame(tkinter.Frame):
    def __init__(self,master):
        tkinter.Frame.__init__(self, master=master, width=500, height=500)
        self.pack()
        
        self.entry1 = tkinter.Entry(self, text = "Entrer ici")
        self.entry1.pack()
        self.entry1.place(x=10, y=10, width=100, height=50)
        
        # exemple de lecture
        #texte = self.entry1.get()
        # exemple d'écriture
        #self.entry1.config(text = texte)
        
        self.bouton2 = tkinter.Button(self, text = "Appuyer là pour mettre en minuscules", 
                                 command=self.bouton2_click)
        self.bouton2.pack()
        self.bouton2.place(x=10, y=70, width=100, height=50)
        
        self.mainloop()


    def bouton2_click(self):
        print("Hello GUI !")
        texte = self.entry1.get()
        self.entry1.delete(0, "end")
        self.entry1.insert(0, texte.lower())
        
if __name__ == "__main__":
    afficheur = tkinter.Tk()
    fenetre = ModificationFrame(afficheur)
