#include <Python.h>
/*
 * C:\Users\STarikUser\git\Formation_Python\Formation_Python>gcc hello_mod.c
 * -IC:\Users\STarikUser\AppData\Local\Programs\Python\Python38-32\include
 * -LC:\Users\STarikUser\AppData\Local\Programs\Python\Python38-32\libs
 * -lpython38 -shared -m32 -o hello_mod.pyd*/
static PyObject* say_hello(PyObject* self, PyObject* args)
{
    const char* name;
    if (!PyArg_ParseTuple(args, "s", &name))
        return NULL;
    printf("Bonjour %s!\n", name);
    Py_RETURN_NONE;
}

static PyMethodDef HelloMethods[] =
{
     {"say_hello", say_hello, METH_VARARGS, "Dit bonjour."},
     NULL
};

static struct PyModuleDef hello_module = {
   PyModuleDef_HEAD_INIT,
   "hello_mod",   NULL,
   -1,   HelloMethods,
   NULL, NULL, NULL, NULL
};

PyMODINIT_FUNC PyInit_hello_mod(void)
{
   return PyModule_Create(&hello_module);
}
