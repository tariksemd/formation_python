#coding: utf-8 
import datetime

log = """
2021-03-10 11:43:23 Warning info 1
2021-03-12 21:33:23 Warning info 2
2021-03-15 12:43:23 Error info 3
2021-03-17 11:43:23 Warning info 4
2021-03-18 10:43:23 info info 5
"""


# reafficher le log en remplaçant les dates/heures par "moins d'une heure", "aujourd'hui", "hier"

maintenant = datetime.datetime.now()
hier = maintenant - datetime.timedelta(days=1)
print("*****Hier****** :"+ str(hier)+" *******Aujourdhui****** :"+str(maintenant))
for ligne in log.splitlines():
    if ligne == "": continue
    y = int(ligne[0:4])
    m = int(ligne[5:7])
    d = int(ligne[8:10])
    h = int(ligne[11:13])
    mi = int(ligne[14:16])
    s = int(ligne[18:20])
    t = datetime.datetime(y,m,d,h,mi,s)
    
    if maintenant - t < datetime.timedelta(hours=1):
        ligne = "Moins d'une heure  " + ligne[19:]  
    elif (maintenant.year == t.year) and (maintenant.month == t.month) and (maintenant.day == t.day):
        ligne = "Aujourd'hui        " + ligne[19:] 
    elif (hier.year == t.year) and (hier.month == t.month) and (hier.day == t.day):
        ligne = "Hier               " + ligne[19:] 
    print(ligne)
    
        
        